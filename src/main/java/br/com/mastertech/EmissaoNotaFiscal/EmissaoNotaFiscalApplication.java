package br.com.mastertech.EmissaoNotaFiscal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
@RibbonClients(defaultConfiguration = RibbonConfiguration.class)
public class EmissaoNotaFiscalApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmissaoNotaFiscalApplication.class, args);
	}

}


package br.com.mastertech.EmissaoNotaFiscal;

import br.com.mastertech.EmissaoNotaFiscal.clients.AtualizaSolicitacaoClient;
import br.com.mastertech.EmissaoNotaFiscal.clients.CnpjDTO;
import br.com.mastertech.EmissaoNotaFiscal.clients.CnpjClient;
import br.com.mastertech.EmissaoNotaFiscal.exceptions.ManipuladorDeExcecoes;
import br.com.mastertech.SolicitacaoNotaFiscal.models.Log;
import br.com.mastertech.SolicitacaoNotaFiscal.models.SolicitacaoNfe;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class EmissaoNotaFiscal {

    @Autowired
    CnpjClient cnpjClient;

    @Autowired
    AtualizaSolicitacaoClient atualizaSolicitacaoClient;

    @Autowired
    private KafkaTemplate<String, Log> producer;

    /*https://consultarcnpj.com.br/wp-content/plugins/cnpj-rf/receita-ws.php?cnpj=47960950000121*/

    @KafkaListener(topics = "spec2-cynthia-carvalho-1", groupId = "grupo-1")
    public void receber(@Payload SolicitacaoNfe solicitacaoNfe) {
        if (solicitacaoNfe.getIdentidade().length() == 14) {
            CnpjDTO cnpjDTO = cnpjClient.getCnpj(solicitacaoNfe.getIdentidade());

            if (Double.parseDouble(cnpjDTO.getCapital_social()) > 1000000.00) {
            /*Será  descontado o IRRF (1,5%) , CSLL (3%) , PIS /Cofins (0,65%). Seria R$ 948,50 valor líquido.*/
                solicitacaoNfe.getNfe().setValorInicial(solicitacaoNfe.getValor());
                solicitacaoNfe.getNfe().setValorIRRF(solicitacaoNfe.getValor()*0.015);
                solicitacaoNfe.getNfe().setValorCSLL(solicitacaoNfe.getValor()*0.03);
                solicitacaoNfe.getNfe().setValorCofins(solicitacaoNfe.getValor()*0.0065);
                            }
            else { /*Será descontado somente o IRRF (1,5%) R$ 15,00, valor da NF líquido será R$ 985,00.*/
                solicitacaoNfe.getNfe().setValorInicial(solicitacaoNfe.getValor());
                solicitacaoNfe.getNfe().setValorIRRF(solicitacaoNfe.getValor()*0.015);
                solicitacaoNfe.getNfe().setValorCSLL(0.00);
                solicitacaoNfe.getNfe().setValorCofins(0.00);
                 }
             }
            try {
            solicitacaoNfe.setStatus("complete");

            solicitacaoNfe.getNfe().setValorFinal(solicitacaoNfe.getValor() -
                        solicitacaoNfe.getNfe().getValorIRRF() -
                        solicitacaoNfe.getNfe().getValorCSLL() -
                        solicitacaoNfe.getNfe().getValorCofins());
            System.out.println("Realizará update no registro : " + solicitacaoNfe.getId() + solicitacaoNfe.getIdentidade()+ solicitacaoNfe.getStatus()+ solicitacaoNfe.getValor()+ solicitacaoNfe.getNfe().getId());

            atualizaSolicitacaoClient.atualizar(solicitacaoNfe);
            System.out.println("Atualizacao no banco de dados realizada com sucesso");

            Log log = new Log();
            Date data = new Date();
            log.setLog("[" + data + "]" + "[Emissão]: Solicitação do documento " + solicitacaoNfe.getIdentidade() + " acaba de ser processada e foi gerado o numero de nota fiscal : " + solicitacaoNfe.getNfe().getId());
            System.out.println("Vai enviar ao Kafka :" + log.getLog());
            producer.send("spec2-cynthia-carvalho-2", log);
            System.out.println("Log gravado no Kafka com sucesso");
                       }
            catch (FeignException.FeignClientException e) {throw new ManipuladorDeExcecoes.ErrorUpdateSolicitacao();
            }
   }
}


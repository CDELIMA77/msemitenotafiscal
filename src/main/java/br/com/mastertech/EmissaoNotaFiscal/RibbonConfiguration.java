package br.com.mastertech.EmissaoNotaFiscal;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;

public class RibbonConfiguration {

    @Bean
    public IRule iRule() {
        /* aqui vc pode definir como quer balancear, se por vez, randomico ou por localizacao
        return new WeightedResponseTimeRule();
        return new RoundRobinRule() */
        return new RandomRule();
    }
}

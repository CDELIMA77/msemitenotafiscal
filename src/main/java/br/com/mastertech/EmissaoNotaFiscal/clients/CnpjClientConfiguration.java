package br.com.mastertech.EmissaoNotaFiscal.clients;

import feign.Feign;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class CnpjClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder(){
        return new CnpjClientErrorDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallbackFactory(CnpjClientFallBack::new)
                .build();
        return Resilience4jFeign.builder(decorators);

    }

}
package br.com.mastertech.EmissaoNotaFiscal.clients;

import br.com.mastertech.EmissaoNotaFiscal.security.OAuth2FeignConfiguration;
import br.com.mastertech.SolicitacaoNotaFiscal.models.SolicitacaoNfe;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "solicitanfe", configuration = OAuth2FeignConfiguration.class)
public interface AtualizaSolicitacaoClient {
    @PutMapping("/nfe/atualizar")
    void atualizar(@RequestBody SolicitacaoNfe solicitacaoNfe);
}

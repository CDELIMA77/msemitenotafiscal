package br.com.mastertech.EmissaoNotaFiscal.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cnpj" , url = "https://www.receitaws.com.br/v1/cnpj/", configuration = CnpjClientConfiguration.class)
public interface CnpjClient {
    @GetMapping("{cnpj}")
    CnpjDTO getCnpj(@PathVariable String cnpj) ;
    }


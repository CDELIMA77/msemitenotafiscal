package br.com.mastertech.EmissaoNotaFiscal.clients;

import br.com.mastertech.EmissaoNotaFiscal.exceptions.ManipuladorDeExcecoes;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CnpjClientErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response){
        if (response.status() == 400) { return new ManipuladorDeExcecoes.CnpjNotFoundException(); }
        else   {return errorDecoder.decode(s, response);}
    }
}
package br.com.mastertech.EmissaoNotaFiscal.clients;

import java.io.IOException;
import java.net.ConnectException;

public class CnpjClientFallBack implements CnpjClient {
    private Exception cause;

    CnpjClientFallBack(Exception cause) {
        this.cause = cause;
    }

    @Override
    public CnpjDTO getCnpj(String cnpj){
        if ( cause instanceof IOException || cause instanceof ConnectException || ( cause.getLocalizedMessage() != null && cause.getLocalizedMessage().contains("ClientException") ))
        {
            throw new RuntimeException("Serviço cnpj fora do ar");
        }
        throw (RuntimeException) cause;
    }
}
